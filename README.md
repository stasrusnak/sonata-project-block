Sonata Test Work
=======================

![alt text](https://i.ibb.co/smmCKPC/2019-06-18-02-11-49.png)

First see video demonstration 
--------------
https://youtu.be/cQy6yXGJhCM


Other information
------------------

registerBundles:   
sandbox-build/app/AppKernel.php

config-block:   
sandbox-build/vendor/sonata-project/block-bundle/Resources/config/block.xml

BlockService:   
sandbox-build/vendor/sonata-project/block-bundle/Block/Service/TextPhotoBlockService.php

Block Twig:   
sandbox-build/vendor/sonata-project/block-bundle/Resources/views/Block/block_photo.html.twig

Photo entity:   
sandbox-build/src/AppBundle/Entity/Photo.php


Thank you!
------------------

