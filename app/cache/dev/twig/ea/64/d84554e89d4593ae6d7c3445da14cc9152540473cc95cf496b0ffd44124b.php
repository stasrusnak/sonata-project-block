<?php

/* SonataBlockBundle:Block:block_photo.html.twig */
class __TwigTemplate_ea64d84554e89d4593ae6d7c3445da14cc9152540473cc95cf496b0ffd44124b extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->env->resolveTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_block($context, array $blocks = array())
    {
        // line 5
        echo "    <div id=\"carousel\" class=\"carousel slide\" data-ride=\"carousel\">
        <ol class=\"carousel-indicators\">
            ";
        // line 7
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photo"]) ? $context["photo"] : $this->getContext($context, "photo")));
        foreach ($context['_seq'] as $context["_key"] => $context["photos"]) {
            // line 8
            echo "                <li data-target=\"#carousel\"
                    data-slide-to=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($context["photos"], "id", array()), "html", null, true);
            echo "\" ";
            if (($this->getAttribute($context["photos"], "id", array()) == 1)) {
                echo " class=\"active\" ";
            }
            echo "></li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photos'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "        </ol>
        <div class=\"carousel-inner\">
            ";
        // line 13
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photo"]) ? $context["photo"] : $this->getContext($context, "photo")));
        foreach ($context['_seq'] as $context["_key"] => $context["photos"]) {
            // line 14
            echo "                <div ";
            if (($this->getAttribute($context["photos"], "id", array()) == 1)) {
                echo " class=\"item active\" ";
            } else {
                echo " class=\"item\" ";
            }
            echo ">
                    <h3 class=\"sonata-feed-title\">";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["photos"], "title", array()), "html", null, true);
            echo "</h3>
                    <img class=\"img-responsive center-block\" alt=\"";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["photos"], "title", array()), "html", null, true);
            echo "\" src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["photos"], "link", array()), "html", null, true);
            echo "\"/>
                    <p>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["photos"], "decription", array()), "html", null, true);
            echo "</p>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photos'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "        </div>
        <a style=\"font-size: 40px;\" href=\"#carousel\" role=\"button\" data-slide=\"prev\">
            <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span></a>
        <a style=\"font-size: 40px;\" href=\"#carousel\" role=\"button\" data-slide=\"next\">
            <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span> </a>
    </div>
";
    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_photo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 20,  80 => 17,  74 => 16,  70 => 15,  61 => 14,  57 => 13,  53 => 11,  41 => 9,  38 => 8,  34 => 7,  30 => 5,  27 => 4,  18 => 1,);
    }
}
