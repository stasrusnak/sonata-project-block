<?php

namespace Sonata\BlockBundle\Block\Service;

use Exporter\Source\DoctrineDBALConnectionSourceIterator;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Doctrine\DBAL\Driver\Connection;

/**
 *
 * @author     Stanislav Rusnak <https://www.facebook.com/rusnak.photo>
 */
class TextPhotoBlockService extends BaseBlockService
{
    protected $host = '127.0.0.1:3306';
    protected $db = 'mysqlsandbox';
    protected $user = 'root';
    protected $pass = '00250025';
    protected $charset = 'utf8';

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @return string
     */
    public function getCharset()
    {
        return $this->charset;
    }
    /**
     * {@inheritdoc}
     */


    public function getPhoto()
    {
        $db = new \PDO("mysql:host=".$this->getHost().";dbname=".$this->getDb().";charset=".$this->getCharset(), $this->getUser(), $this->getPass());
        $photo = $db->query("SELECT id,link,title,decription FROM mysqlsandbox.example_picture")->fetchall();
        return $photo;
    }


    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        return $this->renderResponse('SonataBlockBundle:Block:block_photo.html.twig', [
            'block' => $blockContext->getBlock(),
            'photo' => $this->getPhoto()
        ], $response);
    }
}
