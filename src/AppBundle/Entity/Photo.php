<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="photo")
 */
class Photo
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    protected $decription;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $link;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Photo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set author.
     *
     * @param string $author
     *
     * @return Photo
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Set body.
     *
     * @param string $body
     *
     * @return Photo
     */
    public function setgetDecription($decription)
    {
        $this->decription = $decription;

        return $this;
    }

    /**
     * Get body.
     *
     * @return string
     */
    public function getDecription()
    {
        return $this->decription;
    }

    /**
     * Set url.
     *
     * @param string|null $url
     *
     * @return Photo
     */
    public function setUrl($link = null)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string|null
     */
    public function getLink()
    {
        return $this->link;
    }
}
